//  AUTHOR: Rob Tillaart
// VERSION: 0.1.01
// PURPOSE: simulator to test DHT lib
//    DATE: 2014-06-14
//     URL:
//
// Released to the public domain
//  
// Modified by: Abdel-Gany Jr Odedele
//     Version: 0.9 
//        Date: 2019-11-06

const int dataPin = 5;
byte b[5];
void setup()
{
  // Serial.begin(115200);
  /*Serial.print("Start ");
  Serial.println(__FILE__);*/
  noInterrupts();
  pinMode(dataPin, INPUT_PULLUP);
}

void loop()
{
  uint32_t start = 0;
  uint32_t temps = 0;
  // T = -200 .. 1800
  int T = analogRead(A0);
  if (T > 1000)
  {
    T = 1000;               
  }
  // H = 0 .. 1000
  int H = analogRead(A0);
  if (H > 1000)
  {
    H = 1000;               
  }
    /*Serial.print(H);
    Serial.print("\t");
    Serial.println(T);*/
  // WAKE UP SIGNAL DETECTED
  start = micros();
  while(!digitalRead(dataPin)){
      temps = micros() - start ;
  }
  if (temps > 795)
  {
      DHTsend(H, T);
      Serial.print(H);
      Serial.print("\t");
      Serial.println(T);
  }
}


inline void DHTsend(int H, int T)
{
  pinMode(dataPin, OUTPUT);
  // SEND ACK
  digitalWrite(dataPin, LOW);
  delayMicroseconds(77);                  // 80 us
  digitalWrite(dataPin, HIGH);
  delayMicroseconds(70);                  // 80 us
  // PREPARE DATA
  b[0] = H / 256;
  b[1] = H & 255;
  b[2] = 0;
  if (T < 0)
  {
    T = -T;
    b[2] = 0x80;
  }

  b[2] |= T / 256;
  b[3] = T & 255;
  
  // CRC
  b[4] = b[0] + b[1] + b[2] + b[3];

  // SEND DATA
  for (int i = 0; i < 5; i++)
  {
    DHTsendbyte(b[i]);
  }
  // END OF TRANSMISSION SIGNAL
  digitalWrite(dataPin, LOW);
  delayMicroseconds(50);                  // 50 us
  pinMode(dataPin, INPUT_PULLUP);

  // DEBUG
  //  for (int i = 0; i < 5; i++)
  //  {
  //    Serial.print(b[i]);
  //    Serial.print(" ");
  //  }
  //  Serial.println();
}

// timing manual tuned
inline void DHTsendbyte(byte b)
{
  byte mask = 128;
  for(int i = 0; i < 8; i++)
  {
    digitalWrite(dataPin, LOW);
    delayMicroseconds(45);                // 50 us
    digitalWrite(dataPin, HIGH);
    if (b & mask) delayMicroseconds(65);  // 70 us
    else delayMicroseconds(23);           // 26 us
    mask >>= 1;
  }
}
